# README #

### What is this repository for? ###

* This repo contains an android project (eclipse/intelliJ).
An android application to be used by government (or any organization's) survey teams. Users (or surveyors) can collect tax, location and other legal information about establishments located on highways. Specially designed for NHA Pakistan this app was going to be used to keep record of tax and location information of the establishments on motorways and other national highways.

### How do I get set up? ###

* You'll need eclipse or intelliJ to compile and run this project

### Contribution guidelines ###

* We are not interested in contributions to this repo

### Licence ###

* [MIT licence](http://opensource.org/licenses/MIT) applies to this source code. A copy of this licence is included in the source code as licence.txt file.