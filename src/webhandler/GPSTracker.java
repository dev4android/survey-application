package webhandler;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;

public class GPSTracker extends Service implements LocationListener {

	private final Context mContext;
	boolean isGPSEnabled = false;
	boolean canGetLocation = false;
	private Location mlocation = null; // location
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 3; // 5 meters
	private static final long MIN_TIME_BW_UPDATES = 1000; //
    private static final int FIFTEEN_SECONDS = 30000;
    private static long TIME_DIFFERENCE = 0;
	protected LocationManager locationManager;
    public static GPSTracker GPS;

	public GPSTracker(Context context) {
		this.mContext = context;
        GPS = this;
	}
	
    @Override
    public void onCreate() {
        super.onCreate();
    }

	public void startUsingGPS() {
		try {
			locationManager = (LocationManager) mContext
					.getSystemService(LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			Log.e("getLocation", "One");

			if (!isGPSEnabled) {
				isGPSEnabled = false;
			} else {
				this.canGetLocation = true;
				// if GPS Enabled get lat/long using GPS Services
				isGPSEnabled = true;
				locationManager.addGpsStatusListener(mGPSStatusListener);
				Log.e("getLocation", "Two");
					locationManager.requestLocationUpdates(
							LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					Log.e("GPS Enabled", "GPS Enabled");
			}
		} catch (Exception e) {
			Log.e("Location get location ", e.getMessage());
		}
	}

	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(GPSTracker.this);
//			FIRST_FIX = false;
		}
	}

	/**
	 * Function to check GPS enabled
	 * 
	 * @return boolean
	 * */
	public static boolean canGetLocation(Context con) {
		LocationManager locManager = (LocationManager) con
				.getSystemService(LOCATION_SERVICE);
		boolean isGPSEnabled = locManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		return isGPSEnabled;
	}

    public Location getLocation() {
        return mlocation;
    }

    @Override
	public void onLocationChanged(Location location) {
		if (location.getAccuracy()<=20){
            mlocation = location;
            Toast.makeText(
                    mContext,
                    "Location is Updated !Lattitude : " + mlocation.getLatitude()+"Longitude : " + mlocation.getLongitude() +
        				" Accuracy : " + mlocation.getAccuracy(),
                   Toast.LENGTH_SHORT).show();
        }
		Log.e("Location is Lattitude : " + mlocation.getLatitude()
				+ " Longitude : " + mlocation.getLongitude() + " ",
				" Accuracy : " + mlocation.getAccuracy() + " Time : "
						+ mlocation.getTime() + "");
	}

    public boolean isLocationAccurate(){
        if (mlocation!=null){
        long UTCTime = new Date().getTime();

        long locTime = mlocation.getTime();

        long timeDelta = UTCTime - locTime - TIME_DIFFERENCE;
        Log.e("TimeDelta", timeDelta+"");
        if (timeDelta > FIFTEEN_SECONDS){
            return false;
        }
        return true;
        }else{
            return false;
        }
    }

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
        startUsingGPS();
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public Listener mGPSStatusListener = new GpsStatus.Listener() {
		public void onGpsStatusChanged(int event) {
			switch (event) {
			case GpsStatus.GPS_EVENT_STARTED:
				Toast.makeText(mContext, "GPS SEARCHING SETELITE...",
						Toast.LENGTH_SHORT).show();
				Log.e("Searching...: ", "TAG - GPS searching: ");
				break;
			case GpsStatus.GPS_EVENT_STOPPED:
				Log.e("GpsStatus : ", "GPS Stoped");
				break;
			case GpsStatus.GPS_EVENT_FIRST_FIX:
				/*
				 * GPS_EVENT_FIRST_FIX Event is called when GPS is locked
				 */
				Toast.makeText(mContext, "GPS Connected", Toast.LENGTH_SHORT)
						.show();
				if (locationManager != null) {
					mlocation = locationManager
							.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    long UTCTime = new Date().getTime();
                    long locTime = mlocation.getTime();

                    TIME_DIFFERENCE = UTCTime - locTime;

                    Log.e("TimeDelta", TIME_DIFFERENCE+"");
					Log.e("First Fix", "GPS Info:" + mlocation.getLatitude()
							+ ":" + mlocation.getLongitude());
//					FIRST_FIX = true;
				}
				break;
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
				// System.out.println("TAG - GPS_EVENT_SATELLITE_STATUS");
				break;
			}
		}
	};

}
