package webhandler;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Adeel on 2/18/14.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String dbName = "OFFLINE";
    private static final String PLACES = "places";
    private static final String RECORDS = "records";
    private static final int UPLOADING = 0;
    private static final int DONE = 1;


    public DatabaseHelper(Context context) {
        super(context, dbName, null, 63);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Boun,Region,Maintenance_Unit,Row_Use,Noc_Status,Route,Km,Type_Business,Amenity_Id
        db.execSQL("CREATE TABLE " + PLACES + " (id INTEGER UNIQUE , route TEXT , km TEXT, boun TEXT," +
                " region TEXT, maintenace_unit TEXT, row_use TEXT" +
                ", noc TEXT, type_business TEXT, amenity_Id TEXT, lat DOUBLE, lon DOUBLE, image1 TEXT,image2 TEXT,image3 TEXT,image4 TEXT,date_time TEXT,Uid DOUBLE)");
        db.execSQL("CREATE TABLE " + RECORDS + " (id INTEGER UNIQUE , amenity_Id TEXT, status INTEGER, time LONG, placeid INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        db.execSQL("DROP TABLE IF EXISTS " + PLACES);
        db.execSQL("DROP TABLE IF EXISTS " + RECORDS);
        onCreate(db);
    }

    public void savePlace(Place place) {
        insertPlace(place);
    }

    public void deletePlace(int id, Place place) {
        SQLiteDatabase db = this.getWritableDatabase();
        setDoneInLog(id, db, place);
        db.delete(PLACES, "id" + "=?", new String[]{String.valueOf(id)});
        db.close();
    }

    public Bundle getFirstPlace() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + PLACES, new String[]{});

        if (cur.getCount() > 0) {
            try {
                //Log.e("cursorr", DatabaseUtils.dumpCursorToString(cur));
                cur.moveToFirst();
                Place place = new Place(cur.getDouble(17),cur.getString(1), cur.getString(2), cur.getString(3), cur.getString(4),
                        cur.getString(5), cur.getString(8), cur.getString(6), cur.getString(7), cur.getString(9),
                        cur.getDouble(10), cur.getDouble(11), new String[]{cur.getString(12), cur.getString(13), cur.getString(14), cur.getString(15)}, cur.getString(16));
                Bundle bundle = new Bundle();
                bundle.putInt("id", cur.getInt(0));
                bundle.putParcelable("place", place);
                return bundle;
            } catch (Exception e) {
                Log.e("error reading db", e.toString());
                return null;
            }
        }
        return null;
    }

    private void insertPlace(Place place) {
        Log.e("Saving sqlite", "saving...");

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id", getMaxID(db, PLACES));
        cv.put("route", place.getRoute());
        cv.put("Uid", place.getUid());
        Log.e("Saving sqlite1", place.getRoute());
        cv.put("km", place.getKm());
        Log.e("Saving sqlite2", place.getKm());
        cv.put("boun", place.getBoun());
        Log.e("Saving sqlite3", place.getBoun());
        cv.put("region", place.getRegion());
        Log.e("Saving sqlite4", place.getRegion());
        cv.put("maintenace_unit", place.getMaintenance_Unit());
        Log.e("Saving sqlite5", place.getMaintenance_Unit());
        cv.put("row_use", place.getRow_Use());
        Log.e("Saving sqlite6", place.getRow_Use());
        cv.put("noc", place.getNoc_Status());
        Log.e("Saving sqlite7", place.getNoc_Status());
        cv.put("type_business", place.getType_Business());
        Log.e("Saving sqlite8", place.getType_Business());
        if (place.getAmenity_Id().equals("")) {
            Log.e("Saving sqlite", "amenity changed");
            place.setAmenity_Id(getMaxID(db, PLACES) + "-" +
                    place.getMaintenance_Unit() + "-" +
                    place.getRegion() + "-local");
            cv.put("amenity_Id", place.getAmenity_Id());
            Log.e("Saving sqlite", "to " + place.getAmenity_Id());
        } else {
            cv.put("amenity_Id", place.getAmenity_Id());
        }
        cv.put("lat", place.getLat());
        cv.put("lon", place.getLon());
        cv.put("image1", place.getImages()[0]);
        cv.put("image2", place.getImages()[1]);
        cv.put("image3", place.getImages()[2]);
        cv.put("image4", place.getImages()[3]);
        cv.put("date_time", place.getDate_TIme());
        Log.e("Saving sqlite", "Date Ime is :" + place.getDate_TIme());
        db.insert(PLACES, "id", cv);
        Log.e("Saving sqlite", "saved!");
        db.close();
        Log.e("Saving sqlite", "okay done!");

        addToLog(place, cv.getAsInteger("id"));
    }

    public int setPlaceUploaded(int id, Place place, String Amenity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("route", place.getRoute());
        cv.put("Uid", place.getUid());
        cv.put("km", place.getKm());
        cv.put("boun", place.getBoun());
        cv.put("region", place.getRegion());
        cv.put("maintenace_unit", place.getMaintenance_Unit());
        cv.put("row_use", place.getRow_Use());
        cv.put("noc", place.getNoc_Status());
        cv.put("type_business", place.getType_Business());
        // the key is to set the aminety
        cv.put("amenity_Id", Amenity);

        cv.put("lat", place.getLat());
        cv.put("lon", place.getLon());
        cv.put("image1", place.getImages()[0]);
        cv.put("image2", place.getImages()[1]);
        cv.put("image3", place.getImages()[2]);
        cv.put("image4", place.getImages()[3]);
        cv.put("date_time", place.getDate_TIme());
        db.update(PLACES, cv, "id=?",
                new String[]{String.valueOf(id)});
        place.setAmenity_Id(Amenity);
        return id;
    }

    private void addToLog(Place place, int placeId) {
        Log.e("Saving sqlite", "saving...");

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id", getMaxID(db, RECORDS));
        cv.put("amenity_Id", place.getAmenity_Id());
        cv.put("status", UPLOADING);
        cv.put("time", new Date().getTime());
        cv.put("placeid", placeId);
        db.insert(RECORDS, "id", cv);
        Log.e("Saving sqlite", "log saved!");
        db.close();
        Log.e("Saving sqlite", "okay done!");
    }

    private int setDoneInLog(int id, SQLiteDatabase db, Place place) {
        ContentValues cv = new ContentValues();
        cv.put("amenity_Id", place.getAmenity_Id());
        cv.put("status", DONE);
        cv.put("time", new Date().getTime());
        cv.put("placeid", 9999);

        db.update(RECORDS, cv, "placeid=?",
                new String[]{String.valueOf(id)});
        return 1;
    }

    public ArrayList<String[]> get30LogRecords() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + RECORDS + " ORDER BY id DESC LIMIT 100", new String[]{});
        cur.moveToFirst();
        ArrayList<String[]> recs = new ArrayList<String[]>();
        for (int i = 0; i < cur.getCount(); i++) {
            recs.add(new String[]{cur.getString(0), cur.getString(1), cur.getString(2), cur.getString(3)});
            cur.moveToNext();
        }
        return recs;
    }

    private long getMaxID(SQLiteDatabase db, String tableName) {
        int id = 0;
        final String MY_QUERY = "SELECT MAX(id) AS id FROM " + tableName + "";
        Cursor mCursor = db.rawQuery(MY_QUERY, null);
        Log.e("get Max", "getting...");
        if (mCursor.getCount() > 0) {
            Log.e("get Max", "count = " + mCursor.getCount());
            mCursor.moveToFirst();
            id = mCursor.getInt(mCursor.getColumnIndex("id")) + 1;

        }
        Log.e("get Max", "max = " + id);
        return id;
    }

}