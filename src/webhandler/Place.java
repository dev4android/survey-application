package webhandler;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * @author Unique Identifier
 * 
 */
public class Place implements Parcelable {
	private String Boun, Region, Maintenance_Unit, Row_Use, Noc_Status, Route,
			Km, Type_Business, Amenity_Id,Date_TIme;
	private double Lat, Lon,Uid;
    private String[] Images;


    public Place(double uid,String route, String km, String boun, String region,
			String maintenance_unit, String type_business, String type_row_use,
			String noc_status, String amenity_id, double lat, double lon,
			String images[],String date_time) {
		setUid(uid);
    	Route = route;
		Km = km;
		Boun = boun;
		Region = region;
		Maintenance_Unit = maintenance_unit;
		Type_Business = type_business;
		Row_Use = type_row_use;
		Noc_Status = noc_status;
		Amenity_Id = amenity_id;
		Images = images;
		Lat = lat;
		Lon = lon;
        Date_TIme=date_time;
		
	}

	public Place(Parcel in) {
		String[] data = new String[17];

		in.readStringArray(data);
		Route = data[0];
		Km = data[1];
		Boun = data[2];
		Region = data[3];
		Maintenance_Unit = data[4];
		Type_Business = data[5];
		Row_Use = data[6];
		Noc_Status = data[7];
		Amenity_Id = data[8];
		Lat = Double.parseDouble(data[9]);
		Lon = Double.parseDouble(data[10]);
		Images[0] = data[11];
        Images[1] = data[12];
        Images[2] = data[13];
        Images[3] = data[14];
        Date_TIme=data[15];
        Uid=Double.parseDouble(data[16]);
	}
	
	public double getUid() {
		return Uid;
	}

	public void setUid(double uid) {
		Uid = uid;
	}

	public String getKm() {
		return Km;
	}
	public void setKm(String km) {
		this.Km=km;
	}

	public String getBoun() {
		return Boun;
	}
	public void setBoun(String boun) {
		this.Boun=boun;
	}

	public String getRegion() {
		return Region;
	}
	public void setRegion(String region) {
		this.Region=region;
	}

	public String getMaintenance_Unit() {
		return Maintenance_Unit;
	}
	public void setMaintenance_Unit(String maintenance_Unit) {
		this.Maintenance_Unit=maintenance_Unit;
	}

	public String getRow_Use() {
		return Row_Use;
	}
	public void setRow_Use(String row_use) {
		this.Row_Use=row_use;
	}

	public String getNoc_Status() {
		return Noc_Status;
	}
	public void setNoc_Status(String noc_Status) {
		this.Noc_Status=noc_Status;
	}

	public String getRoute() {
		return Route;
	}
	public void setRoute(String route) {
		this.Route=route;
	}
	
	public String getType_Business() {
		return Type_Business;
	}
	public void setType_Business(String type_Business) {
		this.Type_Business=type_Business;
	}

	public String getAmenity_Id() {
		return Amenity_Id;
	}
	public void setAmenity_Id(String amenity_Id) {
		this.Amenity_Id=amenity_Id;
	}

	public double getLat() {
		return Lat;
	}
	public void setLat(double lat) {
		this.Lat=lat;
	}

	public double getLon() {
		return Lon;
	}
	public void setLon(double lon) {
		this.Lon=lon;
	}

    public String[] getImages() {
        return Images;
    }

    public void setImages(int index, String image) {
		this.Images[index] = image;
	}

    public void setDate_TIme(String date_tIme)
    {
        this.Date_TIme=date_tIme;
    }
    public String getDate_TIme(){return this.Date_TIme;}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeStringArray(new String[] { Route, Km, Boun, Region,
				Maintenance_Unit, Type_Business, Row_Use, Noc_Status,
				Amenity_Id, String.valueOf(Lat), String.valueOf(Lon),
                Images[0], Images[1], Images[2], Images[3],Date_TIme,String.valueOf(Uid)});
	}

	

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public Place createFromParcel(Parcel in) {
			return new Place(in);
		}

		public Place[] newArray(int size) {
			return new Place[size];
		}
	};
}
