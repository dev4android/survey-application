package webhandler;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class UserFunctions {

	/**
	 * <blockquote>Unique Identifier
	 */
	private JSONParser jsonParser;
    private static String API_URL = "http://u9u.biz/survey/";
	private static String LOGIN_TAG = "login";
	private static String INSERT_TAG = "insert";
	private static String GET_PLACES_TAG = "get";
	private static String SEARCH = "search";
	private static String UPDATE = "update";
    private static String IMAGE = "image";

	// constructor
	public UserFunctions() {
		jsonParser = new JSONParser();
	}

	/**
	 * function make Login Request
	 * 
	 * @param username
	 * @param username
	 * @param password
	 * @throws Exception
	 * */
	public JSONObject loginUser(String username, String password)
			throws Exception {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", LOGIN_TAG));
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("password", password));
		JSONObject json;
		json = jsonParser.getJSONFromUrl(API_URL, params);
		Log.e("JSON", json.toString());
		return json;
	}

	public JSONObject getplaces() throws Exception {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", GET_PLACES_TAG));
		JSONObject json = jsonParser.getJSONFromUrl(API_URL, params);
		Log.e("JSON", json.toString());
		return json;
	}

	public JSONObject search(String search_text) throws Exception {
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", SEARCH));
		params.add(new BasicNameValuePair("search_text", search_text));
		JSONObject json = jsonParser.getJSONFromUrl(API_URL, params);
		Log.e("JSON", json.toString());
		return json;
	}

    public JSONObject uploadImage(String amenity_id, String path, int index) throws Exception {
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", IMAGE));
        params.add(new BasicNameValuePair("amenity_id", amenity_id));
        params.add(new BasicNameValuePair("index", String.valueOf(index)));

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(
                path, options);

        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inDither = true;
        options.inSampleSize = UserFunctions.calculateInSampleSize(options,
                400, 300);

        Bitmap imgbit = null;
        ByteArrayOutputStream stream = null;
        byte[] image_bytes= null;

        imgbit = BitmapFactory.decodeFile(
                path, options);
        stream = new ByteArrayOutputStream();
        imgbit.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        image_bytes = stream.toByteArray();
        params.add(new BasicNameValuePair("image", Base64.encodeToString(
                image_bytes, Base64.DEFAULT)));

        // getting JSON Object
        JSONObject json = jsonParser.getJSONFromUrl(API_URL, params);
        // return json
        return json;
    }

    /**
	 * function make Inser Place Request
	 * 
	 * @throws Exception
	 */

	public JSONObject addplace(Place place) throws Exception {
		// Building Parameters
		Log.e("Add Place", " " + place.getBoun() + " " + place.getRegion()
				+ " " + place.getRoute() + "");
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", INSERT_TAG));
		params.add(new BasicNameValuePair("route", place.getRoute()));
		Log.e("Route is : ", place.getRoute());
		Log.e("Km is : ", place.getKm());
		params.add(new BasicNameValuePair("km", place.getKm()));
		params.add(new BasicNameValuePair("boun", place.getBoun()));
		params.add(new BasicNameValuePair("region", place.getRegion()));
		params.add(new BasicNameValuePair("maintenance_unit", place
				.getMaintenance_Unit()));
		params.add(new BasicNameValuePair("type_business", place
				.getType_Business()));
		params.add(new BasicNameValuePair("type_row_use", place.getRow_Use()));
		params.add(new BasicNameValuePair("noc_status", place.getNoc_Status()));
		params.add(new BasicNameValuePair("amenity_id", place.getAmenity_Id()));
		params.add(new BasicNameValuePair("lat", String.valueOf(place.getLat())));
		params.add(new BasicNameValuePair("lon", String.valueOf(place.getLon())));
        params.add(new BasicNameValuePair("date_time", place.getDate_TIme()));
		// getting JSON Object
		JSONObject json = jsonParser.getJSONFromUrl(API_URL, params);
		// return json

		return json;
	}

	public JSONObject updatePlace(Place place) throws Exception {
		// Building Parameters
		Log.e("update Place", " " + place.getBoun() + " " + place.getRegion()
				+ " " + place.getRoute() + "");
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", UPDATE));
		params.add(new BasicNameValuePair("route", place.getRoute()));
		params.add(new BasicNameValuePair("km", place.getKm()));
		params.add(new BasicNameValuePair("boun", place.getBoun()));
		params.add(new BasicNameValuePair("region", place.getRegion()));
		params.add(new BasicNameValuePair("maintenance_unit", place
				.getMaintenance_Unit()));
		params.add(new BasicNameValuePair("type_business", place
				.getType_Business()));
		params.add(new BasicNameValuePair("type_row_use", place.getRow_Use()));
		params.add(new BasicNameValuePair("noc_status", place.getNoc_Status()));
		params.add(new BasicNameValuePair("amenity_id", place.getAmenity_Id()));
		params.add(new BasicNameValuePair("lat", String.valueOf(place.getLat())));
		params.add(new BasicNameValuePair("lon", String.valueOf(place.getLon())));
        params.add(new BasicNameValuePair("date_time", place.getDate_TIme()));
		// getting JSON Object
		JSONObject json = jsonParser.getJSONFromUrl(API_URL, params);
		// return json

		return json;
	}

	private InputStream OpenHttpConnection(String urlString) throws IOException {
		InputStream in = null;
		int response = -1;

		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();

		if (!(conn instanceof HttpURLConnection))
			throw new IOException("Not an HTTP connection");

		try {
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();
			response = httpConn.getResponseCode();
			if (response == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (Exception ex) {
			throw new IOException("Error connecting");
		}
		return in;
	}

	public Bitmap DownloadImage(String URL) throws Exception {
		Bitmap bitmap = null;
		InputStream in = null;
		URL = API_URL + "images/" + URL;
		Log.e("url", URL);

		in = OpenHttpConnection(URL);
		BufferedInputStream bis = new BufferedInputStream(in, 8190);

		ByteArrayBuffer baf = new ByteArrayBuffer(50);
		System.out.println("BAF= " + baf);
		int current = 0;
		while ((current = bis.read()) != -1) {
			baf.append((byte) current);
		}
		byte[] imageData = baf.toByteArray();
		System.out.println("imageData= " + imageData);
		bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
		in.close();
		return bitmap;
	}

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
