package com.unique.survayapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;
import android.content.pm.ActivityInfo;

/**
 * Created by adeel on 2/21/14.
 */
public class IntroFragmentVideo extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String fileName = "android.resource://"+  getActivity().getPackageName() + "/raw/hj";
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_introvideo, container, false);
        VideoView videoview = (VideoView) rootView.findViewById(R.id.videoView);
        videoview.setMediaController(new MediaController(getActivity()));
        Display display = getActivity().getWindow().getWindowManager().getDefaultDisplay();
        videoview.getHolder().setFixedSize(display.getWidth(), display.getHeight());

        videoview.setVideoPath(fileName);
        videoview.requestFocus();
        videoview.start();
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }
}