package com.unique.survayapplication;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.SearchView;
import webhandler.GPSTracker;
import java.util.List;

/**
 * 
 * @author Unique Identifier
 * 
 **/
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class MainActivity extends FragmentActivity
        implements DashboardFragment.activityInterface{

	FragmentManager manager;
    MenuItem searchItem;

	// GPSTracker gps = null;

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (GPSTracker.GPS != null) {
			GPSTracker.GPS.stopUsingGPS();
			GPSTracker.GPS.stopSelf();
		}
	}
	@Override
	public void onBackPressed() {
        CustomFragment currentFragment = (CustomFragment) getVisibleFragment();
        currentFragment.onGoBack();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		manager = getSupportFragmentManager();

        if (savedInstanceState==null){
            DashboardFragment fragdash = new DashboardFragment();
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            fragmentTransaction.replace(R.id.fraglayout, fragdash);
            Log.e("MainActivity", "dashboard called");
            fragmentTransaction.commit();
        }

		// Starting uploading service : just sending initial request
		this.startService(new Intent(this, DataUploaderService.class));
		// Starting the GPS service;
		if (GPSTracker.GPS == null) {
			GPSTracker.GPS = new GPSTracker(this);
			Log.e("FragmentAdd_P", "Service was null, started");
			GPSTracker.GPS.startUsingGPS();
		}
		if (!GPSTracker.canGetLocation(MainActivity.this)) {
			Log.e("MainAcitivity", "Gps is not enabled");
                showSettingsAlert("GPS settings",
                        "GPS is not enabled. Do you want to go to settings menu?",
                        true, this);
		} else {
			Log.e("MainAcitivity", "Gps is enabled");
			GPSTracker.GPS.startUsingGPS();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.actionbar, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final MenuItem search = menu.findItem(R.id.action_search);
        searchItem = search;
		final SearchView searchView = (SearchView) menu.findItem(
				R.id.action_search).getActionView();
		searchView.setSubmitButtonEnabled(true);
		searchView.setQueryRefinementEnabled(true);
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(false);
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String s) {
				SearchRecentSuggestions suggestions = new SearchRecentSuggestions(
						MainActivity.this,
						RecentSuggestContentprovider.AUTHORITY,
						RecentSuggestContentprovider.MODE);
				suggestions.saveRecentQuery(s, null);
				SearchFragment frag = new SearchFragment();
				Bundle b = new Bundle();
				b.putString("search_text", s);
				frag.setArguments(b);
				FragmentTransaction fragmentTransaction = manager
						.beginTransaction();
				fragmentTransaction.replace(R.id.fraglayout, frag);
				//fragmentTransaction.addToBackStack("search");
				fragmentTransaction.commit();
                searchView.clearFocus();
                search.collapseActionView();
				return true;
			}

			@Override
			public boolean onQueryTextChange(String s) {
				return false;
			}
		});
		searchView
				.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
					@Override
					public boolean onSuggestionSelect(int i) {
						Log.e("search", "select");
						return false;
					}

					@Override
					public boolean onSuggestionClick(int i) {
						Log.e("search", "click " + i);
						Cursor cur = searchView.getSuggestionsAdapter()
								.getCursor();
						cur.moveToPosition(i);
						searchView.setQuery(cur.getString(2), true);
						return true;
					}
				});

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar actions click
        Log.e("options", "In option selected");
		switch (item.getItemId()) {
		case R.id.action_add:
			displayView();
			break;
		case R.id.action_exit:
			onDialogMessage();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return super.onOptionsItemSelected(item);
	}

	public static void showSettingsAlert(String title, String message,
			boolean type, final Context mcontext) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mcontext);

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// On pressing Settings button
		if (type) {
			alertDialog.setPositiveButton("Settings",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(
									Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							mcontext.startActivity(intent);
						}
					});
		}
		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.e("MainAcitivity", "OnactivityResult is called");
	}

	/**
	 * Diplaying add fragment
	 * */
	private void displayView() {
		// update the main content by replacing fragments
		Fragment fragment = new FragmentAddPlace();
		Bundle b = new Bundle();
		b.putString("button", "Save");
		fragment.setArguments(b);

		if (fragment != null) {
			FragmentTransaction fragmentTransaction = manager
					.beginTransaction();
			fragmentTransaction.replace(R.id.fraglayout, fragment);
            fragmentTransaction.addToBackStack("home");
			fragmentTransaction.commit();
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}
    private Fragment getVisibleFragment(){
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            if(fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }
	public void onDialogMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage("Do you want to exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
	}
    public void activateSearch() {
        if (searchItem!=null) {
            Log.e("search", "not null");
            this.onOptionsItemSelected(searchItem);
            searchItem.expandActionView();
            Log.e("search", "activated");
        }else{
            Log.e("search", "NotActivated" + searchItem);
        }
    }
}