package com.unique.survayapplication;

import android.content.Context;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Adeel on 3/10/14.
 */
public class SpecialToast extends Toast {
    public SpecialToast(Context context) {
        super(context);
    }
    /*
    * @param context
    * @param drawable zero
    */
    public static Toast makeText(Context context, String text, int duration, int backgroundColor, int textColor, int drawable){
        SpecialToast specialToast = new SpecialToast(context);
        LinearLayout layout=new LinearLayout(context);
        layout.setBackgroundResource(backgroundColor);

        TextView tv=new TextView(context);
        tv.setTextColor(textColor);
        tv.setTextSize(16);
        tv.setGravity(Gravity.CENTER_VERTICAL);
        tv.setText(text);
        tv.setPadding(15, 5, 15, 5);
        layout.addView(tv);

        if (drawable!=0){
            ImageView img=new ImageView(context);
            img.setImageResource(drawable);
            layout.addView(img);
        }

        specialToast.setView(layout);
        specialToast.setDuration(duration);

        return specialToast;
    }
}
