package com.unique.survayapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import webhandler.*;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author Unique Identifier
 */

public class FragmentAddPlace extends CustomFragment implements
		OnItemSelectedListener, OnClickListener {
	Spinner route, boun, region, maintenance_unit, row_use, noc_status,
			type_business;
	Button btnsave, btngetlocation;

	EditText txtkm;
	TextView txtLoad;
	String[] image_str = new String[4];
	ConnectionDetector cd;
	boolean isInternetPresent;
	ImageView[] imgview = new ImageView[4];
	Bitmap[] imgbit = new Bitmap[4];
	int FIRST_IMAGE = 1, SECOND_IMAGE = 2, THIRD_IMAGE = 3, FOURTH_IMAGE = 4;
	ArrayAdapter<String> spinnerArrayAdapter;
	Place p = null;
	private Uri fileUri;
	public static final int MEDIA_TYPE_IMAGE = 1;
	private static final String IMAGE_DIRECTORY_NAME = "SurveyImages";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.e("OncreateView", "1");
		return inflater.inflate(R.layout.fragment_addplace, container, false);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
        if (fileUri!=null) {
			outState.putParcelable("file_uri", fileUri);
		}
            for (int i = 0; i < imgview.length; i++) {
                try{
				    outState.putParcelable("image" + i, ((BitmapDrawable) imgview[i].getDrawable()).getBitmap());
                }catch (ClassCastException e){
                    outState.putParcelable("image" + i, null);
                }
			}
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.e("onActivity Created", "Activity Started 1");
        //if (savedInstanceState==null)
            init();
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		int checkpref = pref.getInt("km", -1);
		Log.e("Prefrence is :", "" + checkpref);
		if (checkpref != -1) {
			txtkm.setText(checkpref);
		}
		if (getArguments().getParcelable("p") != null) {
			p = getArguments().getParcelable("p");
			String button;
			button = getArguments().getString("button");
			btnsave.setText(button);
			txtkm.setText(p.getKm());
			SetSpinnerValue(boun, p.getBoun());
			SetSpinnerValue(route, p.getRoute());
			// setEdits(maintenance_unit, p.getMaintenance_Unit());
			SetSpinnerValue(noc_status, p.getNoc_Status());
			// setEdits(region, p.getRegion());
			for (int i = 0; i < imgbit.length; i++) {
				new loadImage(i).execute();
			}
			SetSpinnerValue(row_use, p.getRow_Use());
			SetSpinnerValue(type_business, p.getType_Business());
		}
		LoadUserSetting();
		cd = new ConnectionDetector(getActivity());
		isInternetPresent = cd.isConnectingToInternet();
		if (savedInstanceState != null) {
			// get the file url
			fileUri = savedInstanceState.getParcelable("file_uri");
			Log.e("onActivity Created", "URI IS NOT NULL");
            Bitmap bitmap;
			for (int i = 0; i < imgview.length; i++) {
				if ((bitmap = savedInstanceState.getParcelable("image" + i)) != null) {
                    Log.i("imgview is null?", (imgview[i]==null)+"");
					imgview[i].setImageBitmap(bitmap);
				}
			}
		}
		if (GPSTracker.GPS == null) {
			GPSTracker.GPS = new GPSTracker(getActivity());
			GPSTracker.GPS.startUsingGPS();
		}
		Log.e("onActivity Created", "Activity Created");
	}

	private void SetSpinnerValue(Spinner spin, String searchable) {
		Log.e("SearchAble : ", "" + searchable);
		ArrayAdapter adapter = (ArrayAdapter) spin.getAdapter();
		int position = adapter.getPosition(searchable);
		Log.e("Position : ", "" + position);
		spin.setSelection(position);
	}

	private void LoadUserSetting() {
		SecurePreferences securePreferences = new SecurePreferences(
				getActivity(), "NHA", "zetasign3#3", true);
		String rout = securePreferences.getString("route");
		String region = securePreferences.getString("region");
		String uid = securePreferences.getString("uid");
		//String maintenance_unit = securePreferences
				//.getString("maintenance_unit");
		SetSpinnerValue(route, rout);
		SetSpinnerValue(this.region, region);
	    p.setUid(Double.valueOf(uid));
		//SetSpinnerValue(this.maintenance_unit, maintenance_unit);
	}

	private void init() {
		btnsave = (Button) getActivity().findViewById(R.id.btnsave);
		btngetlocation = (Button) getActivity().findViewById(
				R.id.btntakelocation);
		txtLoad = (TextView) getActivity().findViewById(R.id.loadingImage);
		imgview[0] = (ImageView) getActivity().findViewById(R.id.imgpic1);
		imgview[1] = (ImageView) getActivity().findViewById(R.id.imgpic2);
		imgview[2] = (ImageView) getActivity().findViewById(R.id.imgpic3);
		imgview[3] = (ImageView) getActivity().findViewById(R.id.imgpic4);

		boun = (Spinner) getActivity().findViewById(R.id.spinboun);
		region = (Spinner) getActivity().findViewById(R.id.region);

		maintenance_unit = (Spinner) getActivity().findViewById(
				R.id.maintenance_unit);

		row_use = (Spinner) getActivity().findViewById(R.id.row_use);

		noc_status = (Spinner) getActivity().findViewById(R.id.noc_status);

		route = (Spinner) getActivity().findViewById(R.id.route);
		txtkm = (EditText) getActivity().findViewById(R.id.txtkm);
		txtkm.setInputType(InputType.TYPE_CLASS_NUMBER);

		type_business = (Spinner) getActivity()
				.findViewById(R.id.type_business);
		route.setOnItemSelectedListener(this);
		for (int i = 0; i < imgbit.length; i++) {
			imgview[i].setOnClickListener(this);
		}
		btnsave.setOnClickListener(this);
		btngetlocation.setOnClickListener(this);
		boun.setOnItemSelectedListener(this);
		region.setOnItemSelectedListener(this);
		row_use.setOnItemSelectedListener(this);
		noc_status.setOnItemSelectedListener(this);
		type_business.setOnItemSelectedListener(this);
		maintenance_unit.setOnItemSelectedListener(this);
		p = new Place(0,route.getSelectedItem().toString(), txtkm.getText()
				.toString(), boun.getSelectedItem().toString(), "", "",
				type_business.getSelectedItem().toString(), row_use
						.getSelectedItem().toString(), noc_status
						.getSelectedItem().toString(), "", 0, 0, new String[] {
						"", "", "", "" }, "");
		Log.e("Init", "Init Completed");
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		switch (arg0.getId()) {
		case R.id.spinboun:
			p.setBoun(arg0.getItemAtPosition(arg2).toString());
			break;
		case R.id.region:
			p.setRegion(arg0.getItemAtPosition(arg2).toString());
			Log.e("Onitem selected", "Region is selected " + p.getRegion());
			String type[] = null;
			Log.e("Selected item is", "Selected Item" + p.getRegion());
			if (p.getRegion().equals("P-N")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_PN);
			} else if (p.getRegion().equals("P-S")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_PS);
			} else if (p.getRegion().equals("S-N")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_SN);
			} else if (p.getRegion().equals("S-S")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_SS);
			} else if (p.getRegion().equals("KPK")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_KPK);
			} else if (p.getRegion().equals("Quetta B-N")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_QBN);
			} else if (p.getRegion().equals("Khuzdar B-S")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_KBS);
			} else if (p.getRegion().equals("NAS")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_NAS);
			} else if (p.getRegion().equals("GB")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_GB);
			} else if (p.getRegion().equals("Motorways(M-2,M-3)")) {
				type = getResources().getStringArray(
						R.array.maintenance_unit_Motorways);
			}
			Log.e("Onitem selected", "Region is selected 2" + type[0]);
			spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_spinner_dropdown_item, type);
			spinnerArrayAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			maintenance_unit.setAdapter(spinnerArrayAdapter);
			p.setMaintenance_Unit(maintenance_unit.getSelectedItem().toString());
			break;
		case R.id.maintenance_unit:
			p.setMaintenance_Unit(arg0.getItemAtPosition(arg2).toString());
			break;
		case R.id.row_use:
			p.setRow_Use(arg0.getItemAtPosition(arg2).toString());
			break;
		case R.id.noc_status:
			p.setNoc_Status(arg0.getItemAtPosition(arg2).toString());
			break;
		case R.id.type_business:
			p.setType_Business(arg0.getItemAtPosition(arg2).toString());
			Log.d("Onitem selected",
					"Typ Business is selected " + p.getType_Business());
			break;
		case R.id.route:
			String Regiontype[] = null;
			switch (arg2) {
			case 0:
				Regiontype = getResources().getStringArray(R.array.N_5);
				break;
			case 1:
				Regiontype = getResources().getStringArray(R.array.N_10);
				break;
			case 2:
				Regiontype = getResources().getStringArray(R.array.N_15);
				break;
			case 3:
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21:
				Regiontype = getResources().getStringArray(
						R.array.N_20_105_155_255_305_455_655);
				break;
			case 4:
				Regiontype = getResources().getStringArray(R.array.N_25);
				break;
			case 5:
				Regiontype = getResources().getStringArray(R.array.N_35);
				break;
			case 6:
				Regiontype = getResources().getStringArray(R.array.N_40);
				break;
			case 7:
			case 14:
			case 15:
				Regiontype = getResources().getStringArray(R.array.N_90_95_45);
				break;
			case 8:
				Regiontype = getResources().getStringArray(R.array.N_50);
				break;
			case 9:
				Regiontype = getResources().getStringArray(R.array.N_55);
				break;
			case 10:
				Regiontype = getResources().getStringArray(R.array.N_65);
				break;
			case 11:
				Regiontype = getResources().getStringArray(R.array.N_70);
				break;
			case 12:
				Regiontype = getResources().getStringArray(R.array.N_75);
				break;
			case 13:
				Regiontype = getResources().getStringArray(R.array.N_80);
				break;
			case 22:
				Regiontype = getResources().getStringArray(R.array.M_1);
				break;
			case 23:
			case 24:
				Regiontype = getResources().getStringArray(R.array.M_2_3);
				break;
			case 25:
				Regiontype = getResources().getStringArray(R.array.S_1);
				break;
			case 26:
				Regiontype = getResources().getStringArray(R.array.S_2_3);
				break;

			}
			spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_spinner_dropdown_item, Regiontype);
			spinnerArrayAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			region.setAdapter(spinnerArrayAdapter);
			p.setRegion(region.getSelectedItem().toString());
			p.setRoute(arg0.getItemAtPosition(arg2).toString());
			Log.d("Onitem selected", "Typ Business is selected " + p.getRoute());
			break;
		}
	}
    @Override
    public void onGoBack() {
        super.onGoBack();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Close");
        builder.setMessage("Are you sure? Any unsaved data might be lost!");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FragmentManager manager = getActivity()
                        .getSupportFragmentManager();
                FragmentTransaction t = manager.beginTransaction().replace(
                        R.id.fraglayout, new DashboardFragment());
                t.commit();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

// Function that create image file with real size

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	@Override
	public void onClick(View v) {
		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		switch (v.getId()) {
		case R.id.imgpic1:
			Intent i1 = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			i1.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
			startActivityForResult(i1, FIRST_IMAGE);

			break;
		case R.id.imgpic2:
			Intent i2 = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			i2.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
			startActivityForResult(i2, SECOND_IMAGE);

			break;
		case R.id.imgpic3:
			Intent i3 = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			i3.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
			startActivityForResult(i3, THIRD_IMAGE);
			break;
		case R.id.imgpic4:
			Intent i4 = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			i4.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
			startActivityForResult(i4, FOURTH_IMAGE);
			break;

		case R.id.btnsave:
			if (txtkm.getText().toString().isEmpty()) {
				SpecialToast.makeText(getActivity(), "Fill in the KMs!",
						Toast.LENGTH_LONG, R.color.light_red,
						R.color.list_divider, 0).show();
			} else {
				p.setKm(txtkm.getText().toString());
				Calendar c = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat(
						"dd:MMMM:yyyy HH:mm:ss ");
				p.setDate_TIme(sdf.format(c.getTime()));
				if (GPSTracker.GPS == null) {
					GPSTracker gps = new GPSTracker(getActivity());
					gps.startUsingGPS();
					showWaitingAlert();
				} else if (GPSTracker.GPS.isLocationAccurate()) {
					p.setLat(GPSTracker.GPS.getLocation().getLatitude());
					p.setLon(GPSTracker.GPS.getLocation().getLongitude());
					insertplace ins = new insertplace(getActivity());
					ins.execute();
				} else {
					showWaitingAlert();

				}
			}
			break;
		case R.id.btntakelocation:
			if (!GPSTracker.canGetLocation(getActivity())) {
				Toast.makeText(getActivity(), "Please Turn On the GPS",
						Toast.LENGTH_SHORT).show();
			} else {
				if (GPSTracker.GPS == null) {
					GPSTracker gps = new GPSTracker(getActivity());
					gps.startUsingGPS();
					showWaitingAlert();
				} else if (GPSTracker.GPS.isLocationAccurate()) {
					Toast.makeText(
							getActivity(),
							"Your Location is \n Lattitude:"
									+ GPSTracker.GPS.getLocation()
											.getLatitude()
									+ " Longitutde:"
									+ GPSTracker.GPS.getLocation()
											.getLongitude()
									+ " \n Accuracy :"
									+ GPSTracker.GPS.getLocation()
											.getAccuracy(), Toast.LENGTH_SHORT)
							.show();

					break;
				} else {
					showWaitingAlert();
				}
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.e("Fragemnt", "OnactivityResult : Result code is " + resultCode);
		if (resultCode != 0) {
			if (requestCode == FIRST_IMAGE || requestCode == SECOND_IMAGE
					|| requestCode == THIRD_IMAGE
					|| requestCode == FOURTH_IMAGE) {
				Log.e("Fragemnt", "OnactivityResult : RequestCode is "
						+ requestCode);
                new readImage(fileUri, requestCode - 1).execute();
                //fileToImageView(fileUri, requestCode - 1);
			}
		}
	}

	public class insertplace extends AsyncTask<Void, Void, Void> {
		// flag for Internet connection status
		Context c;
		ProgressDialog dialog;
		Boolean tag = false;

		public insertplace(Context con) {
			this.c = con;
			dialog = new ProgressDialog(c);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (dialog.isShowing())
				dialog.dismiss();
			if (!tag)
				Toast.makeText(getActivity(),
						"Somthing went wrong can't add entry to Database !",
						Toast.LENGTH_LONG).show();
			else {
				Toast.makeText(
						getActivity(),
						"Entry Added Successfully at Lattitude:"
								+ GPSTracker.GPS.getLocation().getLatitude()
								+ " Longitude:"
								+ GPSTracker.GPS.getLocation().getLongitude()
								+ " .", Toast.LENGTH_LONG).show();
				FragmentManager manager = getActivity()
						.getSupportFragmentManager();
				FragmentTransaction t = manager.beginTransaction().replace(
						R.id.fraglayout, new DashboardFragment());
				t.commit();
			}
		}

		public void showProgressDialog(String title, String message) {
			// Setting Dialog Title
			dialog.setTitle(title);

			// Setting Dialog Message
			dialog.setMessage(message);
			dialog.setCancelable(false);
			// Showing Alert Message
			dialog.show();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog("Please Wait", "Processing...");
		}

		@Override
		protected Void doInBackground(Void... params) {
			DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
			try {
				for (int i = 0; i < image_str.length; i++) {
					p.setImages(i, image_str[i]);
				}
				databaseHelper.savePlace(p);
				getActivity().startService(
						new Intent(getActivity(), DataUploaderService.class));
				// check for response
				tag = true;

			} catch (Exception e) {
				Log.e("DO IN BACKGROUD", e.getMessage());
				if (dialog.isShowing())
					dialog.dismiss();
			}

			return null;
		}
	}

	public class loadImage extends AsyncTask<Void, Void, Void> {
		// flag for Internet connection status
		Boolean tag = false;
		int index;

		public loadImage(int index) {
			this.index = index;

		}

		@Override
		protected void onPreExecute() {
			txtLoad.setText("Loading images...");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			UserFunctions userFunctions = new UserFunctions();
			try {
				imgbit[index] = userFunctions
						.DownloadImage(p.getImages()[index]);
				tag = true;
			} catch (Exception e) {
				tag = false;
				Log.e("DO IN BACKGROUD", e.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (!tag) {
				Toast.makeText(getActivity(),
						"Something went wrong! Images may not exist!",
						Toast.LENGTH_LONG).show();
				txtLoad.setText("");
			} else {
				Uri file_path = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
				File dir = new File(file_path.getPath());
				if (!dir.exists()) {
                    dir.mkdirs();
                }
				File file = new File(dir, p.getImages()[index]);
				FileOutputStream fOut;
				try {
					fOut = new FileOutputStream(file);
					imgbit[index].compress(Bitmap.CompressFormat.PNG, 85, fOut);
					fOut.flush();
					fOut.close();
					fileToImageView(Uri.fromFile(file), index);

					SharedPreferences prefs = PreferenceManager
							.getDefaultSharedPreferences(getActivity());
					SharedPreferences.Editor editor = prefs.edit();
					editor.putInt("km", Integer.parseInt(p.getKm()));
					editor.commit();
					txtLoad.setText("");
				} catch (Exception e) {
					Log.e("OnPostExecute Exception:", e.getMessage());
				}
			}
		}
	}
	private void showWaitingAlert() {
		GPSTracker.GPS.startUsingGPS();
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Connecting...");
		builder.setMessage("Connecting To Setelite Please Wait...");
		builder.setNeutralButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (!GPSTracker.GPS.isLocationAccurate()
						&& alert.isShowing()) {
					SystemClock.sleep(1000);
				}
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (alert.isShowing())
							alert.dismiss();
					}
				});
			}
		}).start();
	}

    private Bitmap fileToImageView(final Uri fileuri, final int index) {
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(fileuri.getPath(), options);

		image_str[index] = fileuri.getPath();
		options.inSampleSize = UserFunctions.calculateInSampleSize(options,
				imgview[index].getWidth(), imgview[index].getHeight());
		options.inJustDecodeBounds = false;
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inDither = true;
        final Bitmap bitmap = BitmapFactory.decodeFile(fileuri.getPath(), options);
		imgbit[index] = null;
        return bitmap;
    }

    private class readImage extends AsyncTask<Void, Void, Bitmap> {
        Uri fileuri; int index;
        readImage(Uri file, int i){
            fileuri = file;
            index = i;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {
            try {
            return fileToImageView(fileUri, index);
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap!=null){
                imgview[index].setImageBitmap(bitmap);
                Log.e("img", "Image set");
            }
        }
	}
}
