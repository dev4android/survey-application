package com.unique.survayapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import webhandler.ConnectionDetector;
import webhandler.GPSTracker;
import webhandler.Place;
import webhandler.UserFunctions;

import java.util.ArrayList;

/**
 * @author Unique Identifier
 */
public class MapFragment extends CustomFragment {
    GoogleMap map;
    JSONObject jason;
    ArrayList<Place> lstplaces;
    ConnectionDetector cd;
    boolean isInternetPresent;
    ImageButton btnrefresh;
    View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (root != null) {
            ViewGroup parent = (ViewGroup) root.getParent();
            if (parent != null)
                parent.removeView(root);
        }
        try {
            root = inflater.inflate(R.layout.map_layout, container, false);
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
            return root;
        }
        return root;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("test", true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("Onactivity Created", "activity Started");
        map = ((SupportMapFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMap();
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        lstplaces = new ArrayList<Place>();
        btnrefresh = (ImageButton) getActivity().findViewById(R.id.btnrefresh);
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        if (savedInstanceState == null) {
            getplaces plac = new getplaces(getActivity());
            plac.execute();

        } else {
            Log.e("On postExecute:", "Going to place the markers");
            placemarkers();
        }
        Log.e("Onactivity Created", "activity Ended");

        btnrefresh.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getplaces plac = new getplaces(getActivity());
                plac.execute();
            }
        });
    }

    @Override
    public void onDestroyView() {
        FragmentManager fm = getFragmentManager();

        Fragment xmlFragment = fm.findFragmentById(R.id.map);
        if (xmlFragment != null) {
            fm.beginTransaction().remove(xmlFragment).commit();
        }
        super.onDestroyView();
    }


    @Override
    public void onGoBack() {
        super.onGoBack();
        FragmentManager manager = getActivity()
                .getSupportFragmentManager();
        FragmentTransaction t = manager.beginTransaction().replace(
                R.id.fraglayout, new DashboardFragment());
        t.commit();
    }


    public class getplaces extends AsyncTask<Void, Void, Void> {
        // flag for Internet connection status
        Context c;
        ProgressDialog dialog;
        Boolean tag = false;

        public getplaces(Context con) {
            this.c = con;
            dialog = new ProgressDialog(c);
        }

        @SuppressWarnings("deprecation")
        public void showAlertDialog(Context context, String title,
                                    String message, Boolean status) {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();

            // Setting Dialog Title
            alertDialog.setTitle(title);

            // Setting Dialog Message
            alertDialog.setMessage(message);

            // Setting alert dialog icon
            // alertDialog.setIcon(R.drawable.fail);

            // Setting OK Button
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (dialog.isShowing())
                dialog.dismiss();
            if (!tag) {
                showAlertDialog(c, "No Internet", "No Internet access", true);

            } else {
                Log.e("On postExecute:", "Going to place the markers");
                placemarkers();
            }
        }

        public void showProgressDialog(Context context, String title,
                                       String message) {
            // Setting Dialog Title
            dialog.setTitle(title);

            // Setting Dialog Message
            dialog.setMessage(message);
            dialog.setCancelable(false);
            // Showing Alert Message
            dialog.show();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showProgressDialog(c, "Please Wait", "Loading Places... ");
        }

        @Override
        protected Void doInBackground(Void... params) {
            UserFunctions functions = new UserFunctions();
            if (isInternetPresent) {
                try {
                    jason = functions.getplaces();
                    // check for login response

                    if (jason.getString("success") != null) {
                        String res = jason.getString("success");
                        if (Integer.parseInt(res) == 1) {
                            JSONArray jasonplaces = jason
                                    .getJSONArray("places");
                            for (int i = 0; i < jasonplaces.length(); i++) {

                                Place p = new Place( Double.parseDouble(jasonplaces
                                        .getJSONObject(i).getString(
                                                "uid")),jasonplaces
                                        .getJSONObject(i).getString("route"),
                                        "", "", jasonplaces.getJSONObject(i)
                                        .getString("region"),
                                        jasonplaces.getJSONObject(i).getString(
                                                "maintenance_unite"), "", "",
                                        "", "", Double.parseDouble(jasonplaces
                                        .getJSONObject(i).getString(
                                                "lat")),
                                        Double.parseDouble(jasonplaces
                                                .getJSONObject(i).getString(
                                                        "lon")), new String[]{"", "", "", ""}, ""
                                );
                                lstplaces.add(p);
                            }
                            tag = true;
                        }
                    } else {
                        Log.e("BACKground", "key is null");
                    }
                } catch (JSONException e) {
                    Log.e("DO IN BACKGROUD", e.getMessage());
                    if (dialog.isShowing())
                        dialog.dismiss();
                } catch (Exception e) {
                    Log.e("Error Do In BACKGROUND", "Eroor" + e.getMessage());
                }
            }

            return null;
        }
    }

    private void placemarkers() {
        if (lstplaces != null) {
            for (int i = 0; i < lstplaces.size(); i++) {

                map.addMarker(new MarkerOptions()
                        .title(lstplaces.get(i).getMaintenance_Unit())
                        .position(
                                new LatLng(lstplaces.get(i).getLat(), lstplaces
                                        .get(i).getLon())
                        )
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.mark_blue))
                        .snippet(
                                "Route:" + lstplaces.get(i).getRoute()
                                        + "Region:"
                                        + lstplaces.get(i).getRegion()
                        ));
            }
            if (GPSTracker.GPS != null && GPSTracker.GPS.getLocation() != null) {
                cemrasetting(GPSTracker.GPS.getLocation().getLatitude(),
                        GPSTracker.GPS.getLocation().getLongitude(), true);
            } else {
                cemrasetting(lstplaces.get(0).getLat(), lstplaces.get(0)
                        .getLon(), false);
            }
        }
    }

    private void cemrasetting(double lat, double lon, boolean mylocation) {
        if (mylocation) {
            map.addMarker(new MarkerOptions()
                    .title("Its You")
                    .position(new LatLng(lat, lon))
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.mark_red))
                    .snippet("Its your current Location"));
        }
        if (GPSTracker.GPS.isLocationAccurate() == true) {
            CameraPosition cemraposition = new CameraPosition.Builder()
                    .target(new LatLng(lat, lon)).zoom(11).tilt(30).build();
            map.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cemraposition));
        }
    }
}
