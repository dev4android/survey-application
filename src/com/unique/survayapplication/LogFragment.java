package com.unique.survayapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import webhandler.DatabaseHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * 
 * 
 * @author Adeel
 * 
 */
public class LogFragment extends CustomFragment implements OnItemClickListener {
	static ArrayList<String[]> records = null;
	ListView lstLog;
	ListAdapter listadapter;
    TextView nodata;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.log_fragment_layout, container,
                false);
        ImageButton imageButton = (ImageButton) v.findViewById(R.id.imgRefresh);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LoadLog(getActivity()).execute();
            }
        });
        lstLog = (ListView) v.findViewById(R.id.list);
        lstLog.setOnItemClickListener(this);
        nodata = (TextView) v.findViewById(R.id.empty);
                Log.e("OnCreateView", "View Created");
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.e("Onactivity Created", "Created");
		super.onActivityCreated(savedInstanceState);

        LoadLog loadLog = new LoadLog(getActivity());
        loadLog.execute();
	}
    @Override
    public void onGoBack() {
        super.onGoBack();
        FragmentManager manager = getActivity()
                .getSupportFragmentManager();
        FragmentTransaction t = manager.beginTransaction().replace(
                R.id.fraglayout, new DashboardFragment());
        t.commit();
    }

	public class LoadLog extends AsyncTask<Void, Void, Void> {
		Context c;
		ProgressDialog dialog;

		public LoadLog(Context con) {
			this.c = con;
			dialog = new ProgressDialog(c);
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog("Please Wait", "Loading...");
		}
        @Override
        protected Void doInBackground(Void... params) {
            DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
            records = databaseHelper.get30LogRecords();
            Log.e("count", records.size()+"");
            return null;
        }
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();

				if (records != null) {
					listadapter = new ListAdapter();
					lstLog.setAdapter(listadapter);
				}if(records.size()==0){
                    nodata.setVisibility(TextView.VISIBLE);
                }else if(records.size()!=0){
                    nodata.setVisibility(TextView.INVISIBLE);
                }
			}

		}
		public void showProgressDialog(String title,
				String message) {
			dialog.setTitle(title);
			dialog.setMessage(message);
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	class ListAdapter extends BaseAdapter {

		public ListAdapter() {
			Log.e("ListAdapter", "ListAdapter");
		}

		@Override
		public int getCount() {
			return records.size();
		}

		@Override
		public Object getItem(int position) {
			return records.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View view = convertView;
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.log_fragment_item, null);

				holder = new ViewHolder();
				holder.tv1 = (TextView) view.findViewById(R.id.tv_id);
				holder.tv2 = (TextView) view
						.findViewById(R.id.tv_amenityid);
                holder.tv3 = (TextView) view
                        .findViewById(R.id.tv_time);
				holder.iv = (ImageView) view
						.findViewById(R.id.iv_icon);
				view.setTag(holder);
			} else {

				holder = (ViewHolder) view.getTag();
			}
//            int n = records.size() - (position + 1);
            int n = position;
            Log.e("position", "" + position);
			holder.tv1.setText(records.get(n)[0]);
			holder.tv2.setText(records.get(n)[1]);
			if (Integer.parseInt(records.get(n)[2])==0){
                holder.iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_uploading));
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.iv.getDrawable();
                frameAnimation.start();
            }else{
                holder.iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_done));
            }
            long itemTime = Long.parseLong(records.get(n)[3]);
            long curTime = new Date().getTime();
            int OneSecond = 1000;
            int OneMinute = 60 * OneSecond;
            int OneHour = 60 * OneMinute;
            int OneDay = 24 * OneHour;
            String time;
            if (curTime - itemTime < OneSecond){
                time = "Just now";
            }else if (curTime - itemTime < OneMinute){
                time = "Few seconds ago";
            }else if (curTime - itemTime < OneHour){
                time = (int)((curTime - itemTime) / OneMinute) + " minute/s ago";
            }else if (curTime - itemTime < OneDay){
                time = (int)((curTime - itemTime) / OneHour) + " hour/s ago";
            }else{
                time = (int)((curTime - itemTime) / OneDay) + " day/s ago";
            }
            holder.tv3.setText(time);
            Log.e("item", "" + records.get(n)[1]);
			return view;
		}
		class ViewHolder {
			TextView tv1;
			TextView tv2;
			ImageView iv;
            TextView tv3;
		}
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	}
}