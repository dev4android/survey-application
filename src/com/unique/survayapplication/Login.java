package com.unique.survayapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import webhandler.ConnectionDetector;
import webhandler.UserFunctions;

/**
 * 
 * 
 * @author Unique Identifier
 * 
 */
public class Login extends Activity {
	protected static final String KEY_SUCCESS = "nothing";
	JSONObject jason;
	EditText txtusername;
	EditText txtpassword;
	TextView tverror;
	ConnectionDetector cd;
	boolean isInternetPresent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
		final Button btnlogin = (Button) findViewById(R.id.btnlogin);
		txtusername = (EditText) findViewById(R.id.txtusername);
		txtpassword = (EditText) findViewById(R.id.txtpassword);

        txtpassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i== EditorInfo.IME_ACTION_DONE){
                    btnlogin.performClick();
                }
                return true;
            }
        });
		tverror = (TextView) findViewById(R.id.tverror);
		cd = new ConnectionDetector(this);
		tverror.setVisibility(TRIM_MEMORY_UI_HIDDEN);
		isInternetPresent = cd.isConnectingToInternet();
		btnlogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				isInternetPresent = cd.isConnectingToInternet();
				if (isInternetPresent) {
					if (TextUtils.isEmpty(txtusername.getText())
							|| TextUtils.isEmpty(txtpassword.getText())) {
						showerror();
						tverror.setText("Please fill into all fields.");
						return;
					} else {
						Userlogin login = new Userlogin(Login.this);
						login.execute();
					}
				} else {
					showerror();
					tverror.setText("No internet access");
				}
			}
		});

		// Loading preference ...
        SecurePreferences securePreferences = new SecurePreferences(this,
                "NHA", "zetasign3#3", true);
        String username = securePreferences.getString("username");
        String password = securePreferences.getString("password");
        if ((username == null || password == null)
                || (username.equals("") || password.equals(""))) {
            showerror();
            tverror.setText("Login session expired!");
        } else {
				/*txtusername.setText(username);
				txtpassword.setText(password);
				new Userlogin(this).execute();*/
            Intent main = new Intent(this, MainActivity.class);
            startActivity(main);
            finish();
        }
    }

	private void showerror() {
		if (!tverror.isActivated()) {
			tverror.setVisibility(BIND_ADJUST_WITH_ACTIVITY);
		}
	}

	public class Userlogin extends AsyncTask<Void, Void, Void> {
		// flag for Internet connection status
		Context c;
		ProgressDialog dialog;
		Boolean tag = false;
		String message;

		public Userlogin(Context con) {
			this.c = con;
			dialog = new ProgressDialog(c);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressDialog(c, "Please Wait", " Logging in ... ");
		}

		@Override
		protected Void doInBackground(Void... params) {
			UserFunctions functions = new UserFunctions();
			try {
				jason = functions.loginUser(txtusername.getText().toString(),
						txtpassword.getText().toString());
				// check for login response

				if (jason.getString("success") != null) {
					String res = jason.getString("success");
					if (Integer.parseInt(res) == 1) {
						// user successfully logged in
						// Store user details in Preference
						JSONArray logindetail = jason
								.getJSONArray("places");
						SecurePreferences securePreferences = new SecurePreferences(
								this.c, "NHA", "zetasign3#3", true);
						securePreferences.put("username", txtusername.getText()
								.toString());
						securePreferences.put("password", txtpassword.getText()
								.toString());
						String uid=logindetail.getJSONObject(0).getString("uid");
						String route=logindetail.getJSONObject(0).getString("route");
						String region=logindetail.getJSONObject(0).getString("region");
						String maintenance_unit=logindetail.getJSONObject(0).getString("maintenance_unit");
						
						securePreferences.put("uid",uid);
						securePreferences.put("route", route);
						securePreferences.put("region", region);
						securePreferences.put("maintenance_unit", maintenance_unit);

						tag = true;

					} else {
						// Error in login
						SecurePreferences securePreferences = new SecurePreferences(
								this.c, "NHA", "zetasign3#3", true);
						securePreferences.put("username", "");
						securePreferences.put("password", "");
						tag = false;

					}
				} else {
					Log.e("BACKground", "key is null");
				}
			} catch (Exception e) {
				Log.e("DO IN BACKGROUD Exception:", e.getMessage());
				message = e.getMessage();
				if (dialog.isShowing())
					dialog.dismiss();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (dialog.isShowing())
				dialog.dismiss();
			if (!tag) {
				showerror();
				tverror.setText("Please check your Internet Connection and try again : -> "
						+ message);
			} else {
				Intent main = new Intent(c, MainActivity.class);
				startActivity(main);
				finish();
			}

		}

		public void showProgressDialog(Context context, String title,
				String message) {
			// Setting Dialog Title
			dialog.setTitle(title);

			// Setting Dialog Message
			dialog.setMessage(message);
			dialog.setCancelable(false);
			// Showing Alert Message
			dialog.show();
		}

	}

}
