package com.unique.survayapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
/**
 * 
 * @author Unique Identifier
 * 
 **/
public class DashboardFragment extends CustomFragment
        implements View.OnClickListener {

	ImageButton btnadd, btnintro, btnlog, btnmap, btnexit, btnabout;
    activityInterface mCallback;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.dashboard_layout, container,
				false);
		return rootView;
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (activityInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnSearchClick");
        }
    }

    @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		init();
	}

   	private void init() {
		btnadd = (ImageButton) getActivity().findViewById(R.id.imgbtnadd);
		btnintro = (ImageButton) getActivity().findViewById(R.id.imgbtnintro);
		btnlog = (ImageButton) getActivity().findViewById(R.id.imgbtnlog);
		btnmap = (ImageButton) getActivity().findViewById(R.id.imgbtnmap);
        btnabout = (ImageButton) getActivity().findViewById(R.id.imgbtnabout);
		btnexit = (ImageButton) getActivity().findViewById(R.id.imgbtnexit);
		btnadd.setOnClickListener(this);
		btnintro.setOnClickListener(this);
        btnabout.setOnClickListener(this);
		btnlog.setOnClickListener(this);
		btnmap.setOnClickListener(this);
		btnexit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Fragment fragment = null;
		switch (v.getId()) {
		case R.id.imgbtnadd:
			fragment = new FragmentAddPlace();
			Bundle b = new Bundle();
			b.putString("button", "Save");
			fragment.setArguments(b);
			break;
		case R.id.imgbtnintro:
			fragment = new HomeFragment();
			break;
		case R.id.imgbtnlog:
			fragment = new LogFragment();
			break;
		case R.id.imgbtnmap:
			fragment = new MapFragment();
			break;
        case R.id.imgbtnabout:
            mCallback.activateSearch();
            break;
		case R.id.imgbtnexit:
            userLogout();
			break;
		}
		if (fragment != null) {
			FragmentManager manager = getActivity().getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = manager
					.beginTransaction();
			fragmentTransaction.replace(R.id.fraglayout, fragment);
            fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}
    private void userLogout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("logout");
        builder.setMessage("Do you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new SecurePreferences(getActivity(),
                        "NHA", "zetasign3#3", true).clear();
                getActivity().finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

    @Override
    public void onGoBack() {
        mCallback.onDialogMessage();
    }

    public interface activityInterface {
        public void activateSearch();
        public void onDialogMessage();
    }

}