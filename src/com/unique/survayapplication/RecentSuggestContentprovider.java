package com.unique.survayapplication;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Adeel on 2/15/14.
 */
public class RecentSuggestContentprovider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.unique.survayapplication.RecentSuggestContentprovider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public RecentSuggestContentprovider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
