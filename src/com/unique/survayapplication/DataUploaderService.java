package com.unique.survayapplication;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import org.apache.http.conn.HttpHostConnectException;
import org.json.JSONObject;

import webhandler.DatabaseHelper;
import webhandler.Place;
import webhandler.UserFunctions;

import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by adeel on 2/17/14.
 */
public class DataUploaderService extends IntentService {
    Handler mHandler;

    public DataUploaderService() {
        super("DataUploaderService");
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        Log.e("Uploading", "start");

        while (databaseHelper.getFirstPlace() != null) {
            Log.e("Uploading", "sleep in loop");
            SystemClock.sleep(5000);
            Bundle bundle = databaseHelper.getFirstPlace();
            Place place = bundle.getParcelable("place");
            int id = bundle.getInt("id");
            Log.e("Uploading", "get first");
            UserFunctions userFunctions = new UserFunctions();
            if (place.getAmenity_Id().contains("local")) {
                Log.e("Uploading", "local exist");
                try {
                    Log.e("Uploading", "uploading");
                    JSONObject jason = userFunctions.addplace(place);
                    if (jason.getString("success") != null) {
                        Log.e("Uploading", "success1");
                        String res = jason.getString("success");
                        if (Integer.parseInt(res) == 1) {
                            Log.e("Uploading", "success2");
                            String amenity_id = jason.getString("amenity_id");
                            Log.e("uploading", "amenityId obtained" + amenity_id);
                            int i = 0;
                            while (i < place.getImages().length) {
                                try {
                                    userFunctions.uploadImage(amenity_id, place.getImages()[i], i + 1);
                                    i++;
                                } catch (NullPointerException npe) {
                                    Log.e("Images", npe.toString() + " Image index " + i);
                                    i++;
                                } catch (HttpHostConnectException hhe) {
                                    Log.e("Images", hhe.toString() + " Image index " + i);
                                    SystemClock.sleep(5000);
                                } catch (SocketException se) {
                                    Log.e("Images", se.toString() + " Image index " + i);
                                    SystemClock.sleep(5000);
                                } catch (UnknownHostException uhe) {
                                    Log.e("Images", uhe.toString() + " Image index " + i);
                                    SystemClock.sleep(5000);
                                }catch (Exception e){
                                    Log.e("Images", e.toString() + " Image index " + i);
                                    //i++;
                                }
                            }
                            try {
                                databaseHelper.setPlaceUploaded(id, place, amenity_id);
                            } catch (Exception e) {
                                Log.e("Setting uploaded", e.toString());
                            }
                            //delete the images and delete from database
                            databaseHelper.deletePlace(id, place);
                            Log.e("Uploading", "deleted!");
                            mHandler.post(new DisplayToast(this, "REMOTE : Record " + place.getAmenity_Id() + " Saved"));
                        } else {
                            Log.e("Uploading", "failed!");
                            continue;
                        }
                    } else {
                        Log.e("Uploading", "failed!");
                        continue;
                    }
                } catch (Exception e) {
                    Log.e("Uploading", "failed!  " + e.toString());
                    continue;
                }

            } else {
                // update on remote db
                Log.e("Uploading", "remote");
                try {
                    Log.e("Uploading", "uploading");
                    JSONObject jason = userFunctions.updatePlace(place);
                    if (jason.getString("success") != null) {
                        Log.e("Uploading", "success1");
                        String res = jason.getString("success");
                        if (Integer.parseInt(res) == 1) {
                            Log.e("Uploading", "success2");
                            String amenity_id = jason.getString("amenity_id");
                            int i = 0;
                            while (i < place.getImages().length) {
                                try {
                                    userFunctions.uploadImage(amenity_id, place.getImages()[i], i + 1);
                                    i++;
                                } catch (NullPointerException npe) {
                                    Log.e("Images", npe.toString() + " Image index " + i);
                                    i++;
                                } catch (HttpHostConnectException hhe) {
                                    Log.e("Images", hhe.toString() + " Image index " + i);
                                    SystemClock.sleep(5000);
                                } catch (SocketException se) {
                                    Log.e("Images", se.toString() + " Image index " + i);
                                    SystemClock.sleep(5000);
                                } catch (UnknownHostException uhe) {
                                    Log.e("Images", uhe.toString() + " Image index " + i);
                                    SystemClock.sleep(5000);
                                }
                            }
                            try {
                                databaseHelper.setPlaceUploaded(id, place, amenity_id);
                            } catch (Exception e) {
                                System.out.print(e);
                            }
                            //delete the images and delete from database
                            databaseHelper.deletePlace(id, place);
                            Log.e("Uploading", "deleted!");
                            mHandler.post(new DisplayToast(this, "REMOTE : Record " + place.getAmenity_Id() + " Updated"));
                        } else {
                            Log.e("Uploading", "failed!");
                            continue;
                        }
                    } else {
                        Log.e("Uploading", "failed!");
                        continue;
                    }
                } catch (Exception e) {
                    Log.e("Uploading", "failed!  " + e.toString());
                    continue;
                }

            }

        }
    }
}