package com.unique.survayapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONObject;
import webhandler.ConnectionDetector;
import webhandler.Place;
import webhandler.UserFunctions;

import java.util.ArrayList;

/**
 * 
 * 
 * @author Unique Identifier
 * 
 */
public class SearchFragment extends CustomFragment implements OnItemClickListener {
	String search_text;
	JSONObject jason;
	ArrayList<Place> lstplaces = null;
	ConnectionDetector cd;
	ListView lstsearch;
	ListAdapter listadapter;
	boolean isInternetPresent;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		search_text = getArguments().getString("search_text");
		Log.e("Search_Text", search_text);
		View v = inflater.inflate(R.layout.search_fragment_layout, container,
				false);
		lstsearch = (ListView) v.findViewById(R.id.lstsearch);
		lstsearch.setOnItemClickListener(this);

		Log.e("OnCreateView", "View Created");
		return v;
	}

	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		alertDialog.show();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.e("Onactivity Created", "Created");
		super.onActivityCreated(savedInstanceState);
		cd = new ConnectionDetector(getActivity());
		listadapter = new ListAdapter();
		if (savedInstanceState == null) {
			lstplaces = new ArrayList<Place>();
			if (!search_text.isEmpty()) {
				isInternetPresent = cd.isConnectingToInternet();
				if (isInternetPresent) {
					SearchPlaces p = new SearchPlaces(getActivity());
					p.execute();
				} else {
					showAlertDialog(getActivity(), "No Internet",
							"No Internet access", true);
				}
			} else {
				Log.e("On Activity Created", "Empty Search_text");
			}
		} else {
			Log.e("Parecel Accepted", "Parecel Accepted");
			lstplaces = savedInstanceState.getParcelableArrayList("p");
			Log.e("Parecel Accepted", "Parecel Accepted"
					+ lstplaces.get(0).getMaintenance_Unit());
			lstsearch.setAdapter(listadapter);
		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList("p", lstplaces);
	}

    @Override
    public void onGoBack() {
        super.onGoBack();
        FragmentManager manager = getActivity()
                .getSupportFragmentManager();
        FragmentTransaction t = manager.beginTransaction().replace(
                R.id.fraglayout, new DashboardFragment());
        t.commit();
    }

	public class SearchPlaces extends AsyncTask<Void, Void, Void> {
		// flag for Internet connection status
		Context c;
		ProgressDialog dialog;

		public SearchPlaces(Context con) {
			this.c = con;
			dialog = new ProgressDialog(c);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			showProgressDialog(c, "Please Wait", "Searching...");
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();

				if (lstplaces != null) {
					Log.e("OnpostExecute", "Place count:"+lstplaces.size());
					listadapter = new ListAdapter();
					Log.e("OnpostExecute", "2");
					lstsearch.setAdapter(listadapter);
					Log.e("OnpostExecute", "3");
				}
			}

		}

		public void showProgressDialog(Context context, String title,
				String message) {
			// Setting Dialog Title
			dialog.setTitle(title);

			// Setting Dialog Message
			dialog.setMessage(message);
			dialog.setCancelable(false);
			// Showing Alert Message
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Log.e("DoinBackground", "1");
			UserFunctions functions = new UserFunctions();
			if (isInternetPresent) {
				try {
					jason = functions.search(search_text);

					// check for login response

					if (jason.getString("success") != null) {
						Log.e("DoinBackground", "2");
						String res = jason.getString("success");
						if (Integer.parseInt(res) == 1) {
							JSONArray jasonplaces = jason
									.getJSONArray("places");
							Log.e("DoinBackground", "3");
							for (int i = 0; i < jasonplaces.length(); i++) {

								Place p = new Place(Double.parseDouble(jasonplaces
										.getJSONObject(i).getString(
												"uid")),jasonplaces
										.getJSONObject(i).getString("route"),
										jasonplaces.getJSONObject(i).getString(
												"km"), jasonplaces
												.getJSONObject(i).getString(
														"boun"), jasonplaces
												.getJSONObject(i).getString(
														"region"), jasonplaces
												.getJSONObject(i).getString(
														"maintenance_unite"),
										jasonplaces.getJSONObject(i).getString(
												"type_business"), jasonplaces
												.getJSONObject(i).getString(
														"type_row_use"),
										jasonplaces.getJSONObject(i).getString(
												"noc_status"), jasonplaces
												.getJSONObject(i).getString(
														"amenity_id"),
										Double.parseDouble(jasonplaces
												.getJSONObject(i).getString(
														"lat")),
										Double.parseDouble(jasonplaces
												.getJSONObject(i).getString(
														"lon")), new String[]{jasonplaces
                                        .getJSONObject(i).getString("image1"),jasonplaces
                                        .getJSONObject(i).getString("image2"),jasonplaces
                                        .getJSONObject(i).getString("image3"),jasonplaces
                                        .getJSONObject(i).getString("image4")},jasonplaces.getJSONObject(i).getString(
                                        "date_time"));
								Log.e("DoinBackground", "4");
								lstplaces.add(p);
							}
						}
					} else {
						Log.e("BACKground", "key is null");
					}
				} catch (Exception e) {
					Log.e("DO IN BACKGROUD", e.getMessage());
					if (dialog.isShowing())
						dialog.dismiss();
				}
			}

			return null;
		}
	}

	class ListAdapter extends BaseAdapter {

		public ListAdapter() {
			Log.e("ListAdapter", "ListAdapter");
			//Log.e("Size", " "+lstplaces.size());
		}

		@Override
		public int getCount() {
			return lstplaces.size();
		}

		@Override
		public Object getItem(int position) {
			return lstplaces.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Log.e("Adapter", "1");
			final ViewHolder holder;
			View view = convertView;
			Log.e("Adapter", "2");
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.search_fragment_item, null);

				holder = new ViewHolder();
				holder.tv_route = (TextView) view.findViewById(R.id.tv_route);
				holder.tvmaintenance_unit = (TextView) view
						.findViewById(R.id.tv_maintenance_unit);
				holder.tv_noc_status = (TextView) view
						.findViewById(R.id.tv_noc_status);
				holder.tv_amenity_id = (TextView) view
						.findViewById(R.id.tv_amenity_id);
				view.setTag(holder);
				Log.e("Adapter", "3");
			} else {

				holder = (ViewHolder) view.getTag();
			}
			holder.tv_route.setText(lstplaces.get(position).getRoute());
			holder.tvmaintenance_unit.setText(lstplaces.get(position)
					.getMaintenance_Unit());
			Log.e("Adapter", " " + lstplaces.get(position).getNoc_Status());
			holder.tv_noc_status.setText(lstplaces.get(position)
					.getNoc_Status());
			holder.tv_amenity_id.setText(lstplaces.get(position)
					.getAmenity_Id());
			return view;
		}

		class ViewHolder {
			TextView tv_route;
			TextView tvmaintenance_unit;
			TextView tv_noc_status;
			TextView tv_amenity_id;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.e("ONitemClick", "list item clicked");
		if (lstplaces != null) {
			Place p = lstplaces.get(arg2);
			Bundle b = new Bundle();
			b.putString("button","Update");
			b.putParcelable("p", p);
			FragmentAddPlace fragplace = new FragmentAddPlace();
			fragplace.setArguments(b);
			FragmentManager manager = getActivity().getSupportFragmentManager();
			FragmentTransaction t = manager.beginTransaction().replace(
					R.id.fraglayout, fragplace);
			t.commit();
		}
	}

}
